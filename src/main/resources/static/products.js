var products = []

function findProduct(id) {
    return products[findProductKey(id)];
}

function findProductKey(id) {
    for (var i = 0; i < products.length; i++) {
        if(products[i].id == id) {
            return i;
        }
    }
}

var productService = {
    findById(id, fn) {
        axios.get('/api/v1/products' + id)
            .then(res => fn(res))
            .catch(e => console.log(e))
    }
}

var List = Vue.extend({
    template: '#product-list',
    data: function() {
        return {products: [], searchKey: ''}
    },
    computed: {
        filteredProducts() {
            return this.products.filter((p) => {
                return p.name.indexOf(this.searchKey) > -1
                || p.description.indexOf(this.searchKey) > -1
                || p.price.toString().indexOf(this.searchKey) > -1
            })
        }
    },
    mounted() {
        axios.get('/api/v1/products').then(r => {this.products = r.data; products = r.data}).catch(e => console.log(e))
    },
    methods: {
        deleteConfirm: function(id, name) {
            if(confirm("Delete product " + name + " ?")) {
                axios.delete('/api/v1/products/' + id).then(r => router.push('/')).catch(e => console.log(e))
            }
        }
    }
});

var Product = Vue.extend({
    template: '#product',
    data: function() {
        return {product: findProduct(this.$route.params.product_id)}
    }
});

var ProductEdit = Vue.extend({
    template: '#product-edit',
    data: function() {
        return {product: findProduct(this.$route.params.product_id)}
    },
    methods: {
        updateProduct: function() {
            axios.put('/api/v1/products/' + this.product.id, this.product).then(r => router.push('/')).catch(e => console.log(e))
        }
    }
});

var AddProduct = Vue.extend({
    template: '#add-product',
    data: function() {
        return {
            product: {name : '', description : '', price : ''}
        }
    },
    methods: {
        createProduct: function() {
            axios.post('/api/v1/products', this.product).then(r => router.push('/')).catch(e => console.log(e))
        }
    }
});

var router = new VueRouter({
    routes: [
        {path: '/', component: List, name: 'product-list'},
        {path: '/product/:product_id', component: Product, name: 'product'},
        {path: '/product/add', component: AddProduct, name:'product-add'},
        {path: '/product/edit/:product_id/', component: ProductEdit, name: 'product-edit'},
        // {path: '/product/delete/:product_id', component: ProductDelete, name: 'product-delete'}
    ]
});

var App = {}

new Vue({
    router
}).$mount('#app')

