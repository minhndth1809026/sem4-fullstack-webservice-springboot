package com.example.fullstackwebservicespringboot.repository;

import com.example.fullstackwebservicespringboot.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long> {
}
