package com.example.fullstackwebservicespringboot.api;

import com.example.fullstackwebservicespringboot.dto.ProductDTO;
import com.example.fullstackwebservicespringboot.entity.Product;
import com.example.fullstackwebservicespringboot.mapper.ProductMapper;
import com.example.fullstackwebservicespringboot.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/products")
@RequiredArgsConstructor
public class ProductAPI {
    private final ProductService productService;
//    private final ProductMapper productMapper;

    @GetMapping
    public ResponseEntity<List<Product>> findAll() {
        return ResponseEntity.ok(productService.findAllProduct());
    }

//    @GetMapping
//    public ResponseEntity<List<ProductDTO>> findAll() {
//        return ResponseEntity.ok(productMapper.toProductDTOs(productService.findAllProduct()));
//    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Product p) {
        return ResponseEntity.ok(productService.saveProduct(p));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findById(@PathVariable Long id) {
        Optional<Product> p = productService.findProductById(id);
        if (!p.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(p.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> update(@PathVariable Long id, @Valid @RequestBody Product pro) {
        Optional<Product> p = productService.findProductById(id);
        if (!p.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(productService.saveProduct(pro));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Product> delete(@PathVariable Long id) {
        Optional<Product> p = productService.findProductById(id);
        if (!p.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        productService.deleteProductById(id);
        return ResponseEntity.ok().build();
    }
}
