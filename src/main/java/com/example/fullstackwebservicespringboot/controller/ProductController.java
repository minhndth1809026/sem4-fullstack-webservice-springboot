package com.example.fullstackwebservicespringboot.controller;

import com.example.fullstackwebservicespringboot.api.ProductAPI;
import com.example.fullstackwebservicespringboot.entity.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class ProductController {

    @GetMapping("/")
    public String index(Model model) {
        return "products";
    }
}
