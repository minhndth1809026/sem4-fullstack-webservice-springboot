package com.example.fullstackwebservicespringboot.service;

import com.example.fullstackwebservicespringboot.entity.Product;
import com.example.fullstackwebservicespringboot.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;

    public List<Product> findAllProduct() {
        return productRepository.findAll();
    }

    public Optional<Product> findProductById(Long id) {
        return productRepository.findById(id);
    }

    public Product saveProduct(Product p) {
        return productRepository.save(p);
    }

    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }
}
