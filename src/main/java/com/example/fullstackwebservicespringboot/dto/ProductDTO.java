package com.example.fullstackwebservicespringboot.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductDTO {
    private String nameDTO;
    private String descriptionDTO;
    private BigDecimal priceDTO;
}
